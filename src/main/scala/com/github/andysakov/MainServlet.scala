package com.github.andysakov

import com.github.andysakov.services._
import org.scalatra._
// JSON-related libraries
import org.json4s.{DefaultFormats, Formats}

// JSON handling support from Scalatra
import org.scalatra.json._

class MainServlet extends ScalatraServlet with JacksonJsonSupport {

  protected implicit lazy val jsonFormats: Formats = DefaultFormats

  before() {
    contentType = formats("json")
  }


  get("/org/:name/contributors") {
    val org = params("name")
    ApiService.run(org) match {
      case Left(value) => Left(value)
      case Right(value) =>
        value
          .flatten
          .groupBy(_.name)
          .map(
            dev => Contributor( dev._1, dev._2.map(_.contributions).sum )
          )
          .toList
          .sortBy(_.contributions)
    }
  }

}
