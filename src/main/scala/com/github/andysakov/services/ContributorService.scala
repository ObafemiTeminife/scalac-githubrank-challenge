package com.github.andysakov.services

import com.github.andysakov._

import scala.util.{Failure, Success}

/*
* Factory for [[org.github.andysakov.ContributorService]] instances which are responsible for receiving and parsing api data on contributors*/
object ContributorService extends Service {

  /*
  * Gets the contributors on a list of repos
  * @param repos the repos to obtain contributors for
  * */
  def getContributors(repos: List[Repository]): List[List[Contributor]] = {
    repos map {
      repo =>
        logger.info(s"----Getting contributor listing for repo `${repo.org}/${repo.name}`")
        ApiService.call(s"repos/${repo.org}/${repo.name}/contributors") match {
          case Success(responseText) =>
            implicit val response: String = responseText.text
            val results: List[(String, Int)] = _contributorNames(jsonResponse) zip _contributions(jsonResponse)
            results.map(v => Contributor(v._1, v._2)).sortBy(_.contributions)
          case Failure(ex) =>
            throw ex
        }
    }
  }
}
