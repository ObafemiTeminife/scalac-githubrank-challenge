package com.github.andysakov.services

import com.github.andysakov._

import scala.util.{Failure, Success}

/*
*
* Factory for [[com.github.andysakov.RepositoryService]] instances
* */
object RepositoryService extends Service {

  /*
  *
  * Retrieves pages from the server to enable pagination
  * @param org the name of the organization to obtain pages for
  * */
  def getPages(org: String): Maybe[String] = {
    ApiService.call(s"orgs/$org/repos") match {
      case Failure(ex) =>
        Left(
          if (ex.getMessage.split("\n").head.contains("403")) {
            ErrorReply(message = "API Rate limit exceeded")
          } else {
            ErrorReply(message = ex.getMessage)
          }
        )
      case Success(value) =>
        Right(value.headers("Link").mkString(s"Link Header for org $org[", ", ", " ]"))
    }
  }

  /*
  *
  * Retrieves repos from the server for a particular organization
  * @param org the name of the organization to obtain repos for
  * */
  def getRepositories(org: String): Maybe[List[Repository]] = {
    ApiService.call(s"orgs/$org/repos") match {
      case Success(responseText) =>
        Right(_repos(jsonResponse(responseText.text)).map(name => Repository(org, name)))
      case Failure(ex) =>
        Left(
          if (ex.getMessage.split("\n").head.contains("403")) {
            ErrorReply(message = "API Rate limit exceeded")
          } else {
            ErrorReply(message = ex.getMessage)
          }
        )
    }
  }
}
