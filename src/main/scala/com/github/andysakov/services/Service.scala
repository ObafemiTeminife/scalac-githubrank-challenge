package com.github.andysakov.services

import io.circe._
import io.circe.optics.JsonPath._
import io.circe.parser._
import org.slf4j.{Logger, LoggerFactory}

/*
* A general service describing a feature whose primary purpose is to perform a task and return out put
* */
trait Service {
  protected val logger: Logger =  LoggerFactory.getLogger(getClass)
  protected val API_URL = "https://api.github.com"
  protected val jsonHeaders = Map("Accept" -> "application/vnd.github.v3+json", "Authorization" -> s"token ${sys.env("GH_TOKEN")}")

  protected def jsonResponse(implicit response: String): Json = parse(response).getOrElse(Json.Null)
  
  protected val _repos: Json => List[String] = (_response: Json) =>
    root.each.name.string.getAll(_response)
  protected val _contributorNames: Json => List[String] = (_response: Json) =>
    root.each.login.string.getAll(_response)
  protected val _contributions: Json => List[Int] = (_response: Json) =>
    root.each.contributions.int.getAll(_response)
}
