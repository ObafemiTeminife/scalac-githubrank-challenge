package com.github.andysakov.services
import com.github.andysakov._
import io.circe.optics.JsonPath._
import requests.Response

import scala.util.{Failure, Success, Try}

/*
* Factory for [[org.github.andysakov.ApiService]]
* This object is responsible for most of the interaction with the api
* just to limit the amount of places where interaction takes place  */
object ApiService extends Service {

  /*
  * A function that makes a test connection to the api to ensure it's accessible*/
  private def connectTest(): Boolean = {
    logger.info("Performing connection test...")
    requests
      .get(s"$API_URL/organizations", headers = jsonHeaders)
      .statusCode match {
      case 200 => true
      case _   => false
    }
  }

  /*
  * A function that makes a call to the api to obtain some data and return it
  * @param route the api route to call
  * */
  def call(route: String): Try[Response] = {
    logger.info(s"--Calling api route `$route`...")
    Try(
      requests.get(s"$API_URL/$route", headers = jsonHeaders)
    )
  }

  /*
  * A function that makes a call to the api to obtain the rate limit and use count
  * */
  private def getRateLimit: Maybe[Int] = {
    call("rate_limit") match {
      case Failure(exception) =>
        Left(ErrorReply(message = exception.getMessage))
      case Success(value) =>
        root.rate.used.int.getOption(
          jsonResponse(value.text)
        ) match {
          case Some(limit) =>
            logger.info(s"Rate limit is $limit/5000...")
            Right(limit)
          case None =>
            Left(ErrorReply(message = "Error getting the rate limit"))
        }
    }
  }

  /*
  * A function that makes a call to the api to check if the organization exitsts
  * @param the name of the org to check
  * */
  private def organizationExists(name: String): Maybe[Boolean] = {
    logger.info(s"Checking if org $name exists...")
    call(s"orgs/$name") match {
      case Failure(_) =>
        Left(ErrorReply(message = "Error checking if organization exists"))
      case Success(responseText) =>
        root.login.string.getOption(
          jsonResponse(responseText.text)
        ) match {
          case Some(_) => Right(true)
          case None =>
            Left(ErrorReply(message = s"Organization $name not found"))
        }
    }
  }

  /*
  * A function that brings together the whole process of retrieval and parsing of the repo and contributor data
  * @param org the organization to process
  * */
  def run(org: String): Maybe[List[List[Contributor]]] = {
    logger.info(s"Beginning contributor listing process for org `$org`")
    logger.info(s"Connected to api: $connectTest()()")
    getRateLimit match {
      case Left(value) => Left(value)
      case Right(value) => if(value >= 1) {
        organizationExists(org) match {
          case Right(true) =>
            val repos = RepositoryService.getRepositories(org)

            repos match {
              case Left(value) => Left(value)
              case Right(value) => Right(ContributorService.getContributors(value))
            }
          case _ => Left(ErrorReply(message = s"Organization $org not found in github listing"))
        }
      } else {
        Left(ErrorReply(message = "Rate limit reached!"))
      }
    }
  }
}
