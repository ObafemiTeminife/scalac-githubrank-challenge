package com.github

package object andysakov {
  final case class Contributor(name: String, contributions: Int)
  final case class Repository(org: String, name: String)
  final case class ErrorReply(error: Boolean = true, message: String)
  final case class ApiReply(message: String, documentation_url: String)
  type Maybe[A] = Either[ErrorReply, A]
}
