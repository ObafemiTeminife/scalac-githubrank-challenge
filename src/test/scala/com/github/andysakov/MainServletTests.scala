package com.github.andysakov

import org.scalatra.test.scalatest._

class MainServletTests extends ScalatraFunSuite {

  addServlet(classOf[MainServlet], "/*")

  test("GET / on MainServlet should return status 404") {
    get("/") {
      status should equal (404)
    }
  }

  test("GET /org/github/contributors on MainServlet should return status 200") {
    get( "/org/github/contributors") {
      status should equal (200)
    }
  }

//  override def header =
}
