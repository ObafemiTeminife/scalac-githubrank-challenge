val CirceVersion = "0.14.1"
val ScalatraVersion = "2.8.0"

ThisBuild / scalaVersion := "2.13.6"
ThisBuild / organization := "com.andysakov"
ThisBuild / evictionErrorLevel := Level.Info

lazy val hello = (project in file("."))
  .settings(
    name := "Scalac Github Rank Challenge",
    version := "0.1.0-SNAPSHOT",
    libraryDependencies ++= Seq(
      "org.scalatra" %% "scalatra" % ScalatraVersion,
      "org.scalatra" %% "scalatra-scalatest" % ScalatraVersion % "test",
      "ch.qos.logback" % "logback-classic" % "1.2.3" % "runtime",
      "org.eclipse.jetty" % "jetty-webapp" % "9.4.35.v20201120" % "container",
      "javax.servlet" % "javax.servlet-api" % "3.1.0" % "provided",
      "com.lihaoyi" %% "requests" % "0.6.5",
      "org.scalatra" %% "scalatra-json" % "2.7.0",
      "org.json4s"   %% "json4s-jackson" % "3.6.10",
      "ch.qos.logback" % "logback-classic" % "1.2.3" % "runtime"
    ),
    libraryDependencies ++= Seq(
      "io.circe" %% "circe-core",
      "io.circe" %% "circe-generic",
      "io.circe" %% "circe-parser",
      "io.circe" %% "circe-optics"
    ).map(_ % CirceVersion)
  )

enablePlugins(SbtTwirl)
enablePlugins(JettyPlugin)
