# Scalac Github Rank Challenge #

## Build & Run ##

```sh
$ cd scalac-github-rank-challenge
$ sbt
> jetty:start
> browse
```

If `browse` doesn't launch your browser, manually open [http://localhost:8080/](http://localhost:8080/) in your browser.

## Note ##
Some of the libraries used have some weird bugs in them, like
 - The plugin for sbt environment variables etc...  
## Please ignore them and wait for the following output or similar before sending a request  
```log
2021-08-30 02:07:29.101:INFO:oejs.AbstractConnector:main: Started ServerConnector@5d11346a{HTTP/1.1, (http/1.1)}{0.0.0.0:8080}
2021-08-30 02:07:29.102:INFO:oejs.Server:main: Started @6119ms
```